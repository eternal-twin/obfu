package etwin;

import haxe.macro.Context;
import haxe.macro.Expr;

/**
  Helper class for obfuscation.

  String literals need to be protected when interfacing with external data
  such as: HTTP results, native APIs (flash and loader modules), script
  values, user-visible identifiers, etc.

  String literals that aren't valid identifier names are not obfuscated, so
  there is no need to protect them.

  Example:
  ```
  // Identifier-like string literals should be protected before being displayed
  console.log("hello"); // Prints an obfuscated value
  console.log(Obfu.raw("hello")); // Prints `hello`
  // `Hello, World!` is not a valid identifier so it is never obfuscated:
  console.log("Hello, World!"); // Prints `Hello, World!`
  console.log(Obfu.raw("Hello, World!")); // Prints `Hello, World!`
  ```
 **/
class Obfu {
  /**
    Protects a string literal or an object literal against obfuscation.

    **Usage:**
    ```hx
    Obfu.raw("hello");
    Obfu.raw({ foo: 3, bar: 5 });
    ```
   **/
  public macro static function raw(e: Expr): Expr {
    var expr = switch (e.expr) {
      case EConst(CString(id)):
        EConst(CString(protect(id)));
      case EObjectDecl(fields):
        for (field in fields) {
          field.field = protect(field.field);
        }
        EObjectDecl(fields);
      default:
        Context.error("`Obfu.raw` can only be applied to string or object literals", e.pos);
    };

    return { expr: expr, pos: e.pos };
  }

  /**
    Variant of `Reflect.hasField` which protects against obfuscation.
  **/
  public macro static function hasField(o: ExprOf<Dynamic>, field: String): ExprOf<Bool> {
    return macro Reflect.hasField($e{o}, $v{protect(field)});
  }

  /**
    Variant of `Reflect.field` which protects against obfuscation.
  **/
  public macro static function field(o: ExprOf<Dynamic>, field: String): ExprOf<Dynamic> {
    return macro Reflect.field($e{o}, $v{protect(field)});
  }

  /**
    Variant of `Reflect.setField` which protects against obfuscation.
  **/
  public macro static function setField(o: ExprOf<Dynamic>, field: String, value: ExprOf<Dynamic>): ExprOf<Void> {
    return macro Reflect.setField($e{o}, $v{protect(field)}, $e{value});
  }

  /**
    Variant of `Reflect.callMethod` which protects against obfuscation.
  **/
  public macro static function call(o: ExprOf<Dynamic>, field: String, args: Array<ExprOf<Dynamic>>): ExprOf<Dynamic> {
    return macro {
      var __obj = $o;
      Reflect.callMethod(__obj, Reflect.field(__obj, $v{protect(field)}), $a{args});
    };
  }

  #if macro
  private static inline function protect(string: String): String {
    #if obfu
    return string == "" ? "" : "@" + string;
    #else
    return string;
    #end
  }
  #end
}
